const COL_NUM = 6;
const ROW_NUM = 7;
const WIN_NUM = 4;
const IMG_URL = [ //TODO: Pasarho a un xml/json
    ["./img/cell_yellow.png", "./img/cell_yellow_1.png", "./img/cell_yellow_2.png"],
    ["./img/cell_red.png", "./img/cell_red_1.png", "./img/cell_red_2.png"],
    "./img/cell_bg.png"
]

var board;
var currentPlayer; //0=Yellow 1=Red
var ptsR;
var ptsY;
var f_ended;



//Set up function
function start() {
    console.log("start")
    createBoard()
    currentPlayer = 0;
    ptsR = 0;
    ptsY = 0;
    f_ended = false;
}

//On cell click: 
function jugada(column) {
    var celID

    //If the game ended reset the board and abort current move
    if (f_ended) {
        reset();
        return;
    }

    //Find last free cell in column
    board[column].forEach(function(cel, i) {
        if (cel == -1) {
            celID = i;
        }
    })

    //If the column is full return
    if (celID == undefined) {
        return;
    }


    //Set last free cell ownership to current player
    board[column][celID] = currentPlayer;
    animate(column, celID, currentPlayer)


    $("#c" + column + "r" + celID).addClass(currentPlayer ? "cellR" : "cellY").removeClass("free")


    //Check for win condition
    checkWin(column, celID)

    //change turn
    currentPlayer = !currentPlayer;
}

function createBoard() {
    //Empty board
    $("#game-table").empty();
    board = [COL_NUM, ROW_NUM];

    //Create cols
    for (let index = 0; index < COL_NUM; index++) {
        var colHTML = $("<div>").attr("id", "c" + index).addClass("col"); //"<div id=c3 class=col>"
        $("#game-table").append(colHTML); //Add the col to the game table
        board[index] = []; //Initiate col as empty array
    }

    //Create cels
    $(".col").each(function(colID) { //For each col:

        for (let i = 0; i < ROW_NUM; i++) {
            var rowID = "c" + colID + "r" + i //"c3r4"
            var celHTML = $("<div>").attr("id", rowID) //<div id="c3r2" class="cell">c3r2</div>
                .addClass("cell").addClass("free");


            $(this).append(celHTML); //add cel to row
            board[colID][i] = -1; //set cel value to -1(empty)
        }
    });
    console.log("created");

    //set col on click
    $(".col").click(a => {
        jugada($(a.target).parent().attr("id")[1]); //jugada(column number)
    });
    window.scrollTo(0, document.body.scrollHeight);
}


function checkWin(col, row) {
    //  0 1 2
    //  3   4
    //  5 6 7
    var directions = [
        [-1, -1],
        [0, -1],
        [1, -1],
        [-1, 0],
        [1, 0],
        [-1, 1],
        [0, 1],
        [1, 1]
    ];
    var directionPairs = [ //Direcciones en la misma direccion opuesto sentido
        [0, 7],
        [1, 6],
        [2, 5],
        [3, 4]
    ];

    //Count how many of the current player cells are in the same direction
    directions.forEach(function(dir, index) {
        directions[index] = checkWinDir(col, row, dir[0], dir[1])
    })
    console.log(directions);

    //Add oposite directions and check total number
    directionPairs.forEach(pair => {
        if (directions[pair[0]] + 1 + directions[pair[1]] == WIN_NUM) {
            win(currentPlayer)
        }
    });
}

//Retorna el numero de caselles del jugador actual 
//desde la casella indcada a la distancia indicada
function checkWinDir(col, row, colDel, rowDel) {
    var retVal = 0
    var f_rep = true;
    while (f_rep) {
        var finalCol = Number(col) + Number(colDel * (1 + retVal));
        var finalRow = Number(row) + Number(rowDel * (1 + retVal));
        f_rep = false;

        //Evita els bordes
        if (!(finalCol >= 0 && finalCol < COL_NUM && finalRow >= 0 && finalCol < ROW_NUM)) {
            console.log("col " + finalCol + "  row " + finalRow);
            continue;
        }

        //Comproba el valor, si es positiu repeteix per buscar la seguent
        let val = board[finalCol][finalRow]
        if (val != undefined && val == currentPlayer) {
            retVal++;
            f_rep = true;
        }

    }
    return retVal;
}


function win(winner) {
    f_ended = true;
    alert((winner ? "Red" : "Yellow") + " Won") //TODO: menys cutre
    winner ? $("#ptsR").text("Red: " + ++ptsR) : $("#ptsY").text("Yellow: " + ++ptsY)
}

//Bard reset
function reset() {
    console.log("reset");
    createBoard();
    f_ended = false;
}

//Anima un fitxa de $player caient fins les coordenades $colID-$celID
function animate(colID, celID, player) {
    var inter = setInterval(frame, 30) //cada 30 ms executa frame
    var moves = []
    player = Number(player) //si no agafa player=1 com a player=true;

    //Per a cada una de les celes superiors de la de desti
    for (let i = 0; i < celID; i++) {
        let cellIDStr = "c" + colID + "r" + i;
        moves.push([cellIDStr, IMG_URL[player][1]]) //1r frame
        moves.push([cellIDStr, IMG_URL[player][2]]) //2n frame0
        moves.push([cellIDStr, IMG_URL[2]]) //buit
    }
    //Per la casella de desti:
    moves.push(["c" + colID + "r" + celID, IMG_URL[player][1]]) //1r frame
    moves.push(["c" + colID + "r" + celID, IMG_URL[player][0]]) // ple


    function frame() {
        let move = moves.shift()

        //Si no queden moviments s ha acabat l animacio
        if (move == undefined) {
            clearInterval(inter)
            return;
        }
        //Asignar la imatge en move[1] com a background de la casella amb id move[0]
        $("#" + move[0]).css("background-image", "url(" + move[1] + ")");
    }

}